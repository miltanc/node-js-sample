'use strict';
process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../index');
let data = require('../test/data.json');

chai.use(chaiHttp);

describe('Digital filter', function() {

  // POST /input
  it('should post values in a loop', function() {
    data.input.forEach(function (val, index) {
      chai.request(app)
      .post('/input')
      .send({data: val})
      .end((err, res) => {
          if(err) {
            console.log(err);
          }
          else {
            if(!res.body.error) {
              console.log(res.body.data);
            } else {
              console.log(res.body.message);
            }
          }
      });
    });
    
  });

  // GET /output
  it('should return moving average of stored values', function(done) {
      chai.request(app)
      .get('/output')
      .end((err, res) => {
          done();
          if(err) {
            console.log(err);
          }
          else {
            if(res.body.data) {
              console.log(res.body.data);
            } else {
               console.log(res.body.message);
            }
          }
      });
  });
});
