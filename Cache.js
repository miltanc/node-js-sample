'use strict';

class Cache {
  constructor() {
      this.config = require('./config/config.json');
      this.client = require('redis').createClient(6379, this.config.redis.host, { db: this.config.redis.db });
      this.client.on('connect', function() {
            console.log('connected');
      });
  }
  //store input value to cache
  store(data) {
    return this.client.rpush(['inputs', parseInt(data)], function(err, reply) {
    });
  }
  
  //view cached data
  view(res)
  {
    let returnData = [];
    let client_obj = this.client;
    client_obj.exists('inputs', function(error, reply) {
        if(error) {
            res.status(500).json({error: error, message: 'Error!' });
        }
        else if (reply === 1) {
            client_obj.lrange('inputs', 0, -1, function(err, replies) {
                if(err) {
                    res.status(500).json({error: err, message: 'Error!' });
                }
                else {
                    replies.forEach(function (reply, index) {
                        let prev = index > 0 ? replies[index - 1] : 0;
                        returnData.push((parseInt(reply) + parseInt(prev))/2);
                    });
                    if(returnData.length > 0) {
                        res.status(200).json({error: err, message: 'Record found!', data: returnData });
                        
                    }
                    else {
                        res.status(200).json({error: err, message: 'No record found!' });
                    }
                }
            });
        } else {
            res.status(200).json({error: error, message: 'No record found!' });
        }
    });
  }
}

module.exports = Cache;
