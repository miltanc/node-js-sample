'use strict';
let app = require('express')();
let bodyParser = require('body-parser');
let routes = require('./routes');
let port = 8080;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//  Connect routes to the application
app.use('/', routes);

// Turn on that server!
app.listen(port, () => {
  console.log('App listening on port ' + port);
});

module.exports = app;