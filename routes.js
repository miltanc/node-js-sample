'use strict';
const routes = require('express').Router();
let Cache = require('./Cache');
let cache = new Cache();

routes.post('/input', function (req, res) {
  let result = cache.store(req.body.data);
  let responseArr = {error: false, message: 'Inserted successfully!', data: req.body.data};
  if(!result) {
    responseArr.message = 'Error occured!';
    responseArr.error = true;
  }
  res.status(200).json(responseArr);
});

routes.get('/output', function (req, res) {
   cache.view(res);
});

module.exports = routes;
